package dk.kea.androidclass.a2016_android_todo.view;

import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import dk.kea.androidclass.a2016_android_todo.Entities.Todo;
import dk.kea.androidclass.a2016_android_todo.Model.TodoModel;
import dk.kea.androidclass.a2016_android_todo.R;
import dk.kea.androidclass.a2016_android_todo.Tools.AlertManager;
import dk.kea.androidclass.a2016_android_todo.Tools.ListAdapter;
import dk.kea.androidclass.a2016_android_todo.Tools.SessionManager;

/**
 * Created by mikjensen on 19/05/16.
 */
public class TodoFragment extends ListFragment
{
    public View view;
    public TodoModel todoModel;
    public SessionManager session;
    public AlertManager alert = new AlertManager();
    private ArrayList<Todo> todos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_todo, container, false);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.showAlertTodo(getContext(), "add", null, todoModel, session);
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        session = new SessionManager(getContext());
        todoModel = new TodoModel(this);
        if(session.IsLoggedin())
        {
            todoModel.getTodos(session.getUserData().getToken());
        }

    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        super.onListItemClick(l, v, position, id);

        if(todos.get(position).isAchieved())
        {
            todoModel.updateTodo(session.getUserData().getToken(), todos.get(position).getId(), "", "", false);
        }
        else
        {
            todoModel.updateTodo(session.getUserData().getToken(), todos.get(position).getId(), "", "", true);
        }

    }

    public void todosLoaded(ArrayList<Todo> todos)
    {
        this.todos = todos;
        setListAdapter(new ListAdapter(this, todos));
    }

    public void todoListChanged(String type, String msg)
    {
        todoModel.getTodos(session.getUserData().getToken());
        Toast.makeText(getContext(), type+": "+msg, Toast.LENGTH_LONG).show();
    }
    public void deleteTodo(Todo todo)
    {
        todoModel.deleteTodo(session.getUserData().getToken(), todo.getId());
    }
    public void todoRequestError(String type, String msg)
    {
        Toast.makeText(getContext(), type+": "+msg, Toast.LENGTH_LONG).show();
    }
}
