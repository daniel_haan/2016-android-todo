package dk.kea.androidclass.a2016_android_todo.Model;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import dk.kea.androidclass.a2016_android_todo.Tools.SessionManager;
import dk.kea.androidclass.a2016_android_todo.view.LoginActivity;

/**
 * Created by Simonsays on 5/21/2016.
 */
public class UserModel
{
    private LoginActivity loginActivity;

    public UserModel(LoginActivity loginActivity)
    {
        this.loginActivity = loginActivity;
    }

    public void userLogin(String username, String password)
    {
        new AsyncLoginTask(this).execute(username, password);
    }

    public void getUser(String token)
    {
        new AsyncGetUserTask(this).execute(token);
    }

    public void registerUser(String username, String password, String fullname, String age)
    {
        new AsyncRegisterUserTask().execute(username, password, fullname, age);
    }

    public LoginActivity getLoginActivity() {
        return loginActivity;
    }

    //Here we demonstrate use of an inner class
    public class AsyncRegisterUserTask extends AsyncTask<String, Integer, JSONObject>
    {

        private String username = "";

        private static final String URL_REGISTER = "http://194.239.172.19/api/user/register";

        @Override
        protected JSONObject doInBackground(String... params)
        {
            JSONObject json = null;

            //Setting username, so we can return it to the view, if register was a success
            username = params[0];

            try
            {
                URL url = new URL(URL_REGISTER);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);

                //type
                con.setRequestMethod("POST");

                //Parameters for the request
                String urlParameters = "username=" + params[0] + "&password=" + params[1] + "&fullname=" + params[2] + "&age=" + params[3];

                //Create Data output stream to send data to the api, now that the connection has been configured
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                Log.e("responseCode-RegUser", responseCode +"");

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null)
                {
                    response.append(inputLine);
                }
                in.close();

                return new JSONObject(String.valueOf(response));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


            return null;
        }

        //Called when execution in background is complete
        protected void onPostExecute(JSONObject json)
        {
            try
            {
                Boolean success = json.getBoolean("success");
                String msg = json.getString("msg");
                Log.e("response-msg", msg);

                loginActivity.registerWasSuccessful(success, username, msg);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }


}




