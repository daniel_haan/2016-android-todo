package dk.kea.androidclass.a2016_android_todo.view;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import dk.kea.androidclass.a2016_android_todo.R;
import dk.kea.androidclass.a2016_android_todo.Tools.SessionManager;

/**
 * Created by mikjensen on 19/05/16.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener
{
    SessionManager session;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        session = new SessionManager(getContext());
        Button logoutBtn = (Button) view.findViewById(R.id.logoutBtn);
        logoutBtn.setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.logoutBtn)
        {
            session.logoutUser(getActivity());
        }
    }
}
