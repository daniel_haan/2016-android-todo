package dk.kea.androidclass.a2016_android_todo.Tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import dk.kea.androidclass.a2016_android_todo.Entities.User;
import dk.kea.androidclass.a2016_android_todo.view.LoginActivity;

/**
 * Created by mikjensen on 19/05/16.
 */
public class SessionManager
{
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    int private_mode = 0;

    private static final String PREF_NAME = "todoPref";
    private static final String IS_LOGIN = "Er Logget Ind";

    public SessionManager(Context context)
    {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, private_mode);
        editor = pref.edit();
    }
    public void CreateLoginSession(User user)
    {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString("User", json);
        editor.commit();
    }
    public boolean IsLoggedin()
    {
        return pref.getBoolean(IS_LOGIN, false);
    }
    public void CheckLoggedIn(Activity activity)
    {
        if(!this.IsLoggedin())
        {
            //If not logged in, create an intent, and change activity to login
            Intent i = new Intent(context, LoginActivity.class);

            //Change the activity
            context.startActivity(i);
            activity.finish();
        }
    }
    public User getUserData()
    {
        Gson gson = new Gson();
        String json = pref.getString("User", "");
        User user = gson.fromJson(json, User.class);
        return user;
    }
    public void logoutUser(Activity activity)
    {
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);

        //Change the activity
        context.startActivity(i);
        activity.finish();
    }
}
