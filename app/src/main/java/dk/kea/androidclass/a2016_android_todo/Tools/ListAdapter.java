package dk.kea.androidclass.a2016_android_todo.Tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dk.kea.androidclass.a2016_android_todo.Entities.Todo;
import dk.kea.androidclass.a2016_android_todo.Model.TodoModel;
import dk.kea.androidclass.a2016_android_todo.R;
import dk.kea.androidclass.a2016_android_todo.view.TodoFragment;

/**
 * Created by mikjensen on 21/05/16.
 */
public class ListAdapter extends BaseAdapter implements View.OnClickListener
{
    // http://androidexample.com/How_To_Create_A_Custom_Listview_-_Android_Example/index.php?view=article_discription&aid=67&aaid=92
    public static LayoutInflater inflater = null;

    private TodoFragment tf;
    private ArrayList<Todo> todos;

    public ListAdapter(TodoFragment tf, ArrayList<Todo> todos)
    {
        this.tf = tf;
        this.todos = todos;
        inflater = ( LayoutInflater )tf.getContext().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return todos.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public void onClick(View v)
    {
//        if(todos[getItem(position)])
    }

    public class Holder
    {
        Button editBtn;
        TextView todoLabel;
        ImageView achievedImage;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View rowView;
        Holder holder = new Holder();

        //Instanciate view from layout xml file
        rowView = inflater.inflate(R.layout.list_todo, null);

        //Assign elements from xml to holder fields
        holder.editBtn = (Button) rowView.findViewById(R.id.editBtn);
        holder.todoLabel = (TextView) rowView.findViewById(R.id.todoLabel);
        holder.achievedImage = (ImageView) rowView.findViewById(R.id.achievedImage);

        //Add icon and action
        holder.editBtn.setBackgroundResource(R.drawable.ic_edit);
        holder.editBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                tf.alert.showAlertEdit(tf, todos.get(position), tf.todoModel, tf.session);
            }
        });

        //set text in label to be todo title
        holder.todoLabel.setText(todos.get(position).getTitle());
        if (todos.get(position).isAchieved())
        {
            holder.achievedImage.setBackgroundResource(R.drawable.ic_checked);
        }
        else
        {
            holder.achievedImage.setBackgroundResource(R.drawable.ic_unchecked);
        }

        //holder is used as tag, so for each row, there is a seperate object defined as tag.
        rowView.setTag(holder);

        return rowView;
    }
}
