package dk.kea.androidclass.a2016_android_todo.Entities;

import java.util.ArrayList;
import java.util.Date;

public class Todo
{
    private String id;
    private String title;
    private boolean achieved;
    private Date date;
    private ArrayList<Todo> children = new ArrayList<>();
    private Todo parent;
    private Todo root;

    public Todo(String id, String title, boolean achieved, Date date)
    {
        this.id = id;
        this.title = title;
        this.achieved = achieved;
        this.date = date;
    }

    public Todo(String id, String title, boolean achieved, Date date, ArrayList<Todo> children, Todo parent, Todo root)
    {
        this.id = id;
        this.title = title;
        this.achieved = achieved;
        this.date = date;
        this.children = children;
        this.parent = parent;
        this.root = root;
    }
    public void addChild(Todo child)
    {
        this.children.add(child);
    }


    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAchieved() {
        return achieved;
    }

    public Date getDate() {
        return date;
    }

    public ArrayList<Todo> getChildren() {
        return children;
    }

    public Todo getParent() {
        return parent;
    }

    public Todo getRoot() {
        return root;
    }

    public void setParent(Todo parent) {
        this.parent = parent;
    }

    public void setRoot(Todo root) {
        this.root = root;
    }
}