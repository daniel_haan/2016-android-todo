package dk.kea.androidclass.a2016_android_todo.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by mikjensen on 19/05/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter
{
    int numberOfTabs;
    public PagerAdapter(FragmentManager fm, int numberOfTabs)
    {
        super(fm);
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position) {
            case 0:
                TodoFragment todo = new TodoFragment();
                return todo;
            case 1:
                SettingsFragment settings = new SettingsFragment();
                return settings;
            default:
                return null;
        }
    }

    @Override
    public int getCount()
    {
        return numberOfTabs;
    }
}
