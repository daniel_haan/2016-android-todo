package dk.kea.androidclass.a2016_android_todo.Model;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import dk.kea.androidclass.a2016_android_todo.Entities.Todo;

/**
 * Created by Simonsays on 5/19/2016.
 *
 * Resource: http://www.mkyong.com/java/how-to-send-http-request-getpost-in-java/
 */
public class AsyncTodoTask extends AsyncTask<String, Integer, JSONObject>
{

    private static final String URL_TODO = "http://194.239.172.19/api/todo";
    private TodoModel todoModel;
    private String requestType;
    private int responseCode;

    public AsyncTodoTask(TodoModel todoModel)
    {
        this.todoModel = todoModel;
    }

    @Override
    protected JSONObject doInBackground(String... params)
    {
       try
        {
            URL url;
            if(params[0] == "DELETE")
            {
                url = new URL(URL_TODO + "/"+params[2]); //params[2] is the id
            }
            else
            {
                url = new URL(URL_TODO);
            }

            requestType = params[0];

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);

            //type
            con.setRequestMethod(requestType);

            //headers
            con.setRequestProperty("authorization", params[1]);

            switch(requestType)
            {
                case "GET":
                    return get(con);
                case "POST":
                    con.setDoOutput(true);
                    return post(con, params[2], params[3], params[4], params[5]);
                case "DELETE":
                    return delete(con);
                case "PUT":
                    con.setDoOutput(true);
                    return put(con, params[2], params[3], params[4], params[5]);
                default:
                    Log.e("Error", "Wrong request type for HTTP request.");
                    break;
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Log.e("Error", "Not intended.... One of the return methods in AsyncTodoTask.java threw a exception");
        return null;
    }


    private BufferedReader makeBufferedReader(int responseCode, HttpURLConnection con) throws IOException {
        if (responseCode != 200)
        {
            return new BufferedReader(new InputStreamReader(con.getErrorStream()));
        }
        else
        {
            return new BufferedReader(new InputStreamReader(con.getInputStream()));
        }
    }

    // ---------- PUT ------------ //
    private JSONObject put(HttpURLConnection con, String id, String title, String date, String achieved) throws IOException, JSONException {
        String urlParameters = "id=" + id + "&title=" + title + "&date=" + date + "&archived=" + achieved;
        sendOutput(urlParameters, con);
        return getResponse(con, "PUT");
    }

    // ---------- DELETE --------- //
    private JSONObject delete(HttpURLConnection con) throws IOException, JSONException
    {
        return getResponse(con, "DELETE");
    }

    // ---------- POST ----------- //
    private JSONObject post(HttpURLConnection con, String title, String date, String root, String parent) throws IOException, JSONException
    {
        String urlParameters = "title=" + title + "&date=" + date + "&root=" + root + "&parent=" + parent;
        sendOutput(urlParameters, con);
        return getResponse(con, "POST");
    }

    // ---------- GET ------------ //
    private JSONObject get(HttpURLConnection con) throws IOException, JSONException
    {
        return getResponse(con, "GET");
    }


    // Sends HTTP output with output stream
    private void sendOutput(String urlParameters, HttpURLConnection con) throws IOException {
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
    }


    //Get the response from with input stream
    private JSONObject getResponse(HttpURLConnection con, String type) throws IOException, JSONException {
        responseCode = con.getResponseCode();
        Log.e("responseCode-" + type, responseCode +"");
        BufferedReader in = makeBufferedReader(responseCode, con);
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();
        Log.e(this.getClass().getName()+" Response", response.toString());
        return new JSONObject(String.valueOf(response));
    }

    //Method which is executed on completion
    protected void onPostExecute(JSONObject json)
    {
        switch(requestType)
        {
            case "GET":
                //Create JsonObject ((GSON version)) by parsing our JSON object
                JsonObject jsonObject = new JsonParser().parse(json.toString()).getAsJsonObject();

                //Convert the json object to an json array
                JsonArray roots = jsonObject.getAsJsonArray("todos");

                //create t odo ArrayList and then add all the items from JsonArray to this regular array
                ArrayList<Todo> todoRoots = new ArrayList<>();
                for (int i = 0; i < roots.size(); i++) {
                    //Add the returned "t odo" object to the arraylist (see method below)
                    todoRoots.add(build(roots.get(i).getAsJsonObject(), null));
                }

                //Add the todos array to the TodoFragment.
                todoModel.getTf().todosLoaded(todoRoots);
                break;
            case "POST":
                if(responseCode == 200)
                {
                    todoModel.getTf().todoListChanged("Opret", "Todo er oprettet");
                }
                else
                {
                    todoModel.getTf().todoRequestError("Opret", "Todo blev ikke oprettet, prøv igen!");
                }
                break;
            case "DELETE":
                if(responseCode == 200)
                {
                    todoModel.getTf().todoListChanged("Slet", "Todo blev slettet");
                }
                else
                {
                    todoModel.getTf().todoRequestError("Slet", "Todo blev ikke slettet, prøv igen!");
                }
                break;
            case "PUT":
                if(responseCode == 200)
                {
                    todoModel.getTf().todoListChanged("Rediger", "Todo er ændret");
                }
                else
                {
                    todoModel.getTf().todoRequestError("Rediger", "Todo blev ikke ændret, prøv igen!");
                }
                break;
            default:
                Log.e("Error", "Wrong request type for HTTP request.");
                break;
        }
    }

    //Build a T odo object from the json Object
    private Todo build(JsonObject currentNode, Todo parent){

        //Get the values for the object
        String id = currentNode.get("_id").getAsString();
        String title = currentNode.get("title").getAsString();
        boolean achieved = currentNode.get("archived").getAsBoolean();

        //create the object
        Todo todo = new Todo(id, title, achieved, null);


        //UNIMPLEMENTED: add the todo parents and roots
        if(parent != null){
            todo.setParent(parent);
            if(parent.getRoot() != null){
                todo.setRoot(parent.getRoot());
            }else {
                todo.setRoot(parent);
            }
        }

        //UNIMPLEMENTED: recursively iterate through tree structure to set children of each todo.
        JsonArray children = currentNode.getAsJsonArray("child");
        for (int i = 0; i < children.size(); i++) {
            todo.getChildren().add(build(children.get(i).getAsJsonObject(), todo));
        }

        return todo;
    }
}
