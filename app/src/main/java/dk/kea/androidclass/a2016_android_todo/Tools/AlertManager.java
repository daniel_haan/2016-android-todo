package dk.kea.androidclass.a2016_android_todo.Tools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import dk.kea.androidclass.a2016_android_todo.Entities.Todo;
import dk.kea.androidclass.a2016_android_todo.Model.TodoModel;
import dk.kea.androidclass.a2016_android_todo.R;
import dk.kea.androidclass.a2016_android_todo.view.LoginActivity;
import dk.kea.androidclass.a2016_android_todo.view.TodoFragment;

/**
 * Created by mikjensen on 21/05/16.
 */
public class AlertManager
{
    public void showAlertTodo(Context context, String type, final Todo todo, final TodoModel todoModel, final SessionManager session)
    {
        //Build the alert dialog in the context
        AlertDialog alertdialog = new AlertDialog.Builder(context).create();

        //inflater for layout in given context
        LayoutInflater factory = LayoutInflater.from(context);

        //Create the view object from the alert_todo.xml file
        //must be final, so you can use it in the onclicklistener
        final View textEntryView = factory.inflate(R.layout.alert_todo, null);

        //reference to the text field in the xml file.
        final EditText input1 = (EditText) textEntryView.findViewById(R.id.todoTitle);

        //set icon and view object for the dialog
        alertdialog.setIcon(R.drawable.ic_edit);
        alertdialog.setView(textEntryView);

        switch(type)
        {
            case "add":
                alertdialog.setTitle("Tilføj todo");
                alertdialog.setButton("Tilføj", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        todoModel.addTodo(session.getUserData().getToken(), input1.getText().toString(), "", "", "");
                    }
                });

                break;
            case "update":
                alertdialog.setTitle("Rediger todo");
                input1.setText(todo.getTitle());
                alertdialog.setButton("Rediger", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        todoModel.updateTodo(session.getUserData().getToken(), todo.getId(), input1.getText().toString(), "", false);
                    }
                });
                break;
        }
        alertdialog.show();
    }
    public void showAlertEdit(final TodoFragment tf, final Todo todo, final TodoModel todoModel, final SessionManager session)
    {
        final AlertDialog alertdialog = new AlertDialog.Builder(tf.getContext()).create();
        alertdialog.setTitle("Tilføj todo");
        alertdialog.setIcon(R.drawable.ic_edit);
        alertdialog.setMessage(todo.getTitle());
        alertdialog.setButton("Rediger todo", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                showAlertTodo(tf.getContext(), "update", todo, todoModel, session);
            }
        });
        alertdialog.setButton2("Slet", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                tf.deleteTodo(todo);
                // TODO: Delete todo
            }
        });
        alertdialog.show();
    }
    public void showRegisterUser(final LoginActivity la)
    {
        //Create the dialog
        final AlertDialog alertdialog = new AlertDialog.Builder(la).create();
        alertdialog.setTitle("Opret ny bruger");
        alertdialog.setIcon(R.drawable.ic_add_user);

        //Object to instanciate a XML file as a view object
        LayoutInflater factory = LayoutInflater.from(la);

        final View textEntryView = factory.inflate(R.layout.alert_register, null);

        //GUI content references in the pop-up
        final EditText usernameField = (EditText)textEntryView.findViewById(R.id.usernameField);
        final EditText fullnameField = (EditText)textEntryView.findViewById(R.id.fullnameField);
        final EditText ageField = (EditText)textEntryView.findViewById(R.id.ageField);
        final EditText passField = (EditText)textEntryView.findViewById(R.id.passField);
        final EditText pass2Field = (EditText)textEntryView.findViewById(R.id.pass2Field);

        //add the view to dialog
        alertdialog.setView(textEntryView);

        alertdialog.setButton("Opret", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //test if all fields have input (not empty)
                if(!usernameField.getText().toString().equals("") && !fullnameField.getText().toString().equals("") &&
                        !ageField.getText().toString().equals("") && !passField.getText().toString().equals("") && !pass2Field.getText().toString().equals(""))
                {
                    //test if password fields are equal
                    if(passField.getText().toString().equals(pass2Field.getText().toString()))
                    {
                        //register user
                        la.registerUser(usernameField.getText().toString(), fullnameField.getText().toString(), ageField.getText().toString(), passField.getText().toString());
                    }
                    else
                    {
                        Toast.makeText(la, "Kodeord skal være ens!", Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    Toast.makeText(la, "Alle felter skal udfyldes!", Toast.LENGTH_LONG).show();
                }

            }
        });
        alertdialog.show();
    }

}
