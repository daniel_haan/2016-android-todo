package dk.kea.androidclass.a2016_android_todo.Model;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Simonsays on 5/21/2016.
 *
 * Resource: http://www.mkyong.com/java/how-to-send-http-request-getpost-in-java/
 */
public class AsyncLoginTask  extends AsyncTask<String, Integer, JSONObject>
{

    private String stringUrlLogin = "http://194.239.172.19/api/user/login";
    private UserModel loginModel;

    public AsyncLoginTask(UserModel loginModel) {
        this.loginModel = loginModel;
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        JSONObject json = null;

        try
        {
            //Create URL object, and create HTTP conn by opening the url connection
            URL url = new URL(stringUrlLogin);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");

            //headers
            con.setRequestProperty("authorization", params[1]);

            String urlParameters = "username=" + params[0] + "&password=" + params[1];

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            Log.e("responseCode", responseCode +"");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null)
            {
                response.append(inputLine);
            }
            in.close();

            //parse response to json
            json = new JSONObject(String.valueOf(response));

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return json;
    }

    //Method which is executed on completion
    protected void onPostExecute(JSONObject json)
    {
        try {
            if(json.getBoolean("success"))
            {
                loginModel.getUser(json.getString("token"));
            }
            else
            {
                loginModel.getLoginActivity().loginWasSuccessful(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
