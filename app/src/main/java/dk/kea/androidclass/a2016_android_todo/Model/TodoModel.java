package dk.kea.androidclass.a2016_android_todo.Model;

import dk.kea.androidclass.a2016_android_todo.view.TodoFragment;


/**
 * Created by Simonsays on 5/19/2016.
 */
public class TodoModel
{
    TodoFragment tf;

    public TodoModel(TodoFragment tf)
    {
        this.tf = tf;
    }

    public TodoFragment getTf()
    {
        return tf;
    }

    public void getTodos(String token)
    {
        new AsyncTodoTask(this).execute("GET", token);
    }

    public void addTodo(String token, String title, String date, String root, String parent)
    {
        new AsyncTodoTask(this).execute("POST", token, title, date, root, parent);
    }

    public void deleteTodo(String token, String id)
    {
        new AsyncTodoTask(this).execute("DELETE", token, id);
    }

    public void updateTodo(String token, String id, String title, String date, Boolean achieved)
    {
        new AsyncTodoTask(this).execute("PUT", token, id, title, date, achieved.toString());
    }


}
