package dk.kea.androidclass.a2016_android_todo.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import dk.kea.androidclass.a2016_android_todo.Model.UserModel;
import dk.kea.androidclass.a2016_android_todo.R;
import dk.kea.androidclass.a2016_android_todo.Tools.AlertManager;
import dk.kea.androidclass.a2016_android_todo.Tools.SessionManager;

public class LoginActivity extends Activity
{
    EditText usernameField;
    EditText passwordField;

    SessionManager session;

    AlertManager alert = new AlertManager();
    UserModel userModel;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login); //set xml file as layout

        //Hide loading image
        findViewById(R.id.loading).setVisibility(View.GONE);

        userModel = new UserModel(this);

        //reference to the 2 textfields in the layout
        usernameField = (EditText) findViewById(R.id.usernameField);
        passwordField = (EditText) findViewById(R.id.passwordField);

        //Instance our class that handles the user being logged in or not (shared preferences)
        session = new SessionManager(getApplicationContext());
    }

    //log in method
    public void loginAction(View view)
    {
        //Login call to the persistence layer ***
        userModel.userLogin(usernameField.getText().toString(), passwordField.getText().toString());

        //Hide keyboard     http://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        //show loading image
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
    }

    //Open register user pop-up
    public void registerUserAction(View view)
    {
        alert.showRegisterUser(this);
    }

    //register user method
    public void registerUser(String username, String fullname, String age, String password)
    {
        //call register user to the model
        userModel.registerUser(username, fullname, age, password);

        //show loading image
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
    }

    public void loginWasSuccessful(Boolean success)
    {
        //Hide loading image
        findViewById(R.id.loading).setVisibility(View.GONE);

        if(success)
        {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }
        else
        {
            Toast.makeText(getBaseContext(), "Brugernavn eller kodeord er forkert!", Toast.LENGTH_LONG).show();
        }
    }

    public void registerWasSuccessful(Boolean success, String username, String msg)
    {
        //Hide loading image
        findViewById(R.id.loading).setVisibility(View.GONE);

        if(success)
        {
            usernameField.setText(username);
            passwordField.setText("");
            passwordField.requestFocus();
            Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Failed: " + msg, Toast.LENGTH_LONG).show();
        }
    }

    public SessionManager getSession() {
        return session;
    }
}
