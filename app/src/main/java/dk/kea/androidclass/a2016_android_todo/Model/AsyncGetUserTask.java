package dk.kea.androidclass.a2016_android_todo.Model;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import dk.kea.androidclass.a2016_android_todo.Entities.User;

/**
 * Created by Simonsays on 5/21/2016.
 *
 * Resource: http://www.mkyong.com/java/how-to-send-http-request-getpost-in-java/
 */
public class AsyncGetUserTask extends AsyncTask<String, Integer, JSONObject> {


    private String stringUrl = "http://194.239.172.19/api/user";
    private UserModel userModel;
    private String token;

    public AsyncGetUserTask(UserModel userModel) {
        this.userModel = userModel;
    }
    @Override
    protected JSONObject doInBackground(String... params) {
        token  = params[0];
        JSONObject json = null;


        try
        {
            URL url = new URL(stringUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //Request header:
            con.setRequestProperty("authorization", token);

            int responseCode = con.getResponseCode();
            Log.e("responseCode", responseCode +"");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null)
            {
                response.append(inputLine);
            }
            in.close();

            json = new JSONObject(String.valueOf(response));

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        return json;
    }


    //Method which is executed on completion
    protected void onPostExecute(JSONObject json)
    {

        if(json == null)
        {
            Log.e("FATAL ERROR:", "json file is null");
        }
        else
        {
            try {
                String fullname;
                int age;

                if(json.has("fullname"))
                    fullname = json.getString("fullname");
                else
                    fullname = "";

                if(json.has("age"))
                    age = json.getInt("age");
                else
                    age = -1;

                String username = json.getString("username");

                //Create session for the new user, who logged in.
                userModel.getLoginActivity().getSession().CreateLoginSession(new User(fullname, age, username, token));

                Log.e("USER INFO", userModel.getLoginActivity().getSession().getUserData().asString());

                userModel.getLoginActivity().loginWasSuccessful(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

}
