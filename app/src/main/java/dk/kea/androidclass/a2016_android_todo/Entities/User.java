package dk.kea.androidclass.a2016_android_todo.Entities;

public class User
{
    private String fullname;
    private int age;
    private String username;
    private String token;

    public User(String fullname, int age, String username, String token)
    {
        this.fullname = fullname;
        this.age = age;
        this.username = username;
        this.token = token;
    }

    public String getFullname()
    {
        return fullname;
    }

    public int getAge()
    {
        return age;
    }

    public String getUsername()
    {
        return username;
    }

    public String getToken()
    {
        return token;
    }

    public String asString()
    {
        return "USER FROM SESSION" +
                "\nName: " + fullname +
                "\nAge: " + age +
                "\nUsername: " + username +
                "\nToken: " + token;
    }

}
